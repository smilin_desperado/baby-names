import Vue from "vue";
import Vuex, { MutationTree } from "vuex";
import { Gender } from "@/types";
import { GENDER_KEY } from "@/constants";

Vue.use(Vuex);

interface IState {
  filter: Gender | null;
}

const mutations: MutationTree<IState> = {
  setFilter(state, gender: Gender | null) {
    localStorage.setItem(GENDER_KEY, gender ? gender.toString() : "");
    state.filter = gender;
  }
};

export default new Vuex.Store({
  state: {
    filter: null
  },
  mutations
});
