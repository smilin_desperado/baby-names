import Vue, {
  Component,
  PluginFunction,
  PluginObject,
  VueConstructor,
  DirectiveFunction,
  DirectiveOptions
} from "vue";

declare module "vue/types/options" {
  export interface ComponentOptions<
    V extends Vue,
    Data = DefaultData<V>,
    Methods = DefaultMethods<V>,
    Computed = DefaultComputed,
    PropsDef = PropsDefinition<DefaultProps>,
    Props = DefaultProps
  > {
    vuetify?: any;
  }
}
