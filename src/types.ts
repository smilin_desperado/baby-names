export interface BabyName {
  name: string;
  gender: Gender;
}

export enum Gender {
  Boy = 1,
  Girl,
  Neutral
}
